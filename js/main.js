var DSSV = [] ; 
function themSV() {
    var sv = layThongTinTuForm() ; 
    DSSV.push(sv) ;   
    var dssvjson = JSON.stringify(DSSV) ; 
    localStorage.setItem('local_DSSV' , dssvjson) ; 
    renderDSSV(DSSV) ;
    lamMoiForm()  
}

// khi người dùng reload thì thông tin vẫn hiển thị 
var dataJson = localStorage.getItem('local_DSSV') ; 
if (dataJson != null) {
    var dataArr = JSON.parse(dataJson) ; 
    DSSV = dataArr.map(function(item){
        var sv = new Sinhvien(item.maSV, item.tenSV , item.emailSV , item.matKhauSV , item.diemToan , item.diemLy , item.diemHoa) ; 
        return sv ; 
    })
    renderDSSV(DSSV) ; 
}

function xoaSV(idSV) {
    var viTri = timKiemViTri(idSV , DSSV) ; 
    DSSV.splice(viTri , 1) ; 
    localStorage.setItem('local_DSSV' , JSON.stringify(DSSV)) ; 
    renderDSSV(DSSV) ; 
}

function suaSV(idSV) {
    var viTri = timKiemViTri(idSV , DSSV) ; 
    document.getElementById('txtMaSV').value = DSSV[viTri].maSV ;
    document.getElementById('txtMaSV').disabled = true; 
    document.getElementById('txtTenSV').value = DSSV[viTri].tenSV ; 
    document.getElementById('txtEmail').value = DSSV[viTri].emailSV ; 
    document.getElementById('txtPass').value = DSSV[viTri].matKhauSV ; 
    document.getElementById('txtDiemToan').value = DSSV[viTri].diemToan ; 
    document.getElementById('txtDiemLy').value = DSSV[viTri].diemLy ; 
    document.getElementById('txtDiemHoa').value = DSSV[viTri].diemHoa ; 
    document.getElementById('index').value = viTri ; 
}

function capNhatSV() {
    document.getElementById('txtMaSV').disabled = false; 
    var index = document.getElementById('index').value ; 
    DSSV[index] = layThongTinTuForm() ; 
    localStorage.setItem('local_DSSV' , JSON.stringify(DSSV)) ; 
    renderDSSV(DSSV) ; 
    lamMoiForm() ; 
    
}

function timKiemSV() {
    var maSV = document.getElementById("txtSearch").value ; 
    var SVCT = [] ; 
    for (var index = 0; index < DSSV.length; index++) {
        var element = DSSV[index];
        if (element.maSV == maSV) {
            SVCT.push(element) ; 
        }
        
    }
    renderDSSV(SVCT) ; 
    
}
