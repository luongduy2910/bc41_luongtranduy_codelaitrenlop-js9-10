function layThongTinTuForm() {
    var _maSV = document.getElementById('txtMaSV').value ;
    var _tenSV = document.getElementById('txtTenSV').value ; 
    var _emailSV = document.getElementById('txtEmail').value ; 
    var _matKhauSV = document.getElementById('txtPass').value ; 
    var _diemToan = document.getElementById('txtDiemToan').value * 1 ; 
    var _diemLy = document.getElementById('txtDiemLy').value * 1 ; 
    var _diemHoa = document.getElementById('txtDiemHoa').value * 1 ;
    return new Sinhvien(_maSV ,
        _tenSV ,
        _emailSV ,
        _matKhauSV ,
        _diemToan ,
        _diemLy ,
        _diemHoa)  ; 
}

function renderDSSV(array) {
    var contentHTML = "" ; 
    for (var index = 0; index < array.length; index++) {
        var element = array[index];
        var contentTr = 
        `
        <tr>
            <td>${element.maSV}</td>
            <td>${element.tenSV}</td>
            <td>${element.emailSV}</td>
            <td>${element.tinhDTB()}</td>
            <td><button onclick = "xoaSV('${element.maSV}')" class = "btn btn-danger mr-2">Delete</button><button onclick = "suaSV('${element.maSV}')" class = "btn btn-primary">Edit</button></td>
        </tr>
        `
        contentHTML += contentTr ; 
    }
    document.getElementById('tbodySinhVien').innerHTML = contentHTML ; 
}

function timKiemViTri(masv , array) {
    var viTri = -1 ; 
    for (var index = 0; index < array.length; index++) {
        var element = array[index];
        if(element.maSV == masv) {
            viTri = index ; 
        }
        
    }
    return viTri ; 
}

function lamMoiForm() {
    document.getElementById('txtMaSV').value = "" ; 
    document.getElementById('txtTenSV').value = "" ; 
    document.getElementById('txtEmail').value = "" ; 
    document.getElementById('txtPass').value = "" ; 
    document.getElementById('txtDiemToan').value = "" ; 
    document.getElementById('txtDiemLy').value = "" ; 
    document.getElementById('txtDiemHoa').value = "" ; 
}